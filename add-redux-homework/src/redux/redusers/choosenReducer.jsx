import { ADD_TO_CHOSEN_GOODS, REMOVE_TO_CHOSEN_GOODS } from "../actions/goodsActions";
const savedChosenGoods = localStorage.getItem('chosenGoods');

const initialValue = {
   
    choosenArr: savedChosenGoods ? JSON.parse(savedChosenGoods) : []
}

const chosenReducer = (state = initialValue, actions) => {
    switch(actions.type) {

        case ADD_TO_CHOSEN_GOODS: {
            
          const copyChosenAr = [...state.choosenArr]

          const newChosenArr = copyChosenAr.map((item) => {
              console.log(item)
              if (item.id === actions.payload.id) {
                return { ...item, count: item.count + 1 };
              }
              return item;
            });
      
            console.log(newChosenArr)
            const index = newChosenArr.findIndex((item) => item.id === actions.payload.id);
      
            if (index === -1) {
              newChosenArr.unshift({ ...actions.payload, count: 1 });
            }
            localStorage.setItem('chosenGoods', JSON.stringify(newChosenArr));
            return { ...state, choosenArr: newChosenArr };
        }


        case REMOVE_TO_CHOSEN_GOODS: {   
          const newCopyChosenAr = [...state.choosenArr]
          const index = newCopyChosenAr.findIndex((item) => item.id === actions.payload.id)

          if (index !== -1) { 
                if (newCopyChosenAr[index].count > 1) {
                  const updatedItemCount = {...newCopyChosenAr[index], count: newCopyChosenAr[index].count - 1}
                  newCopyChosenAr[index] = updatedItemCount
                } else {
                  newCopyChosenAr.splice(index, 1);
                }
          }
          localStorage.setItem('chosenGoods', JSON.stringify(newCopyChosenAr));
          return { ...state, choosenArr: newCopyChosenAr };
        }

      
        default: return state
    }
}

export default chosenReducer;