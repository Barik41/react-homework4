import React, {memo} from "react";
import Card from "../Card/Card";
import {shallowEqual, useSelector } from "react-redux";

const ItemsContainer = () => {

    const items = useSelector(state => state.goods.goodsArr, shallowEqual)

    return(
        <section>
            <div className="cardWrap">
            <h2> Laptops</h2>
                {items && items.map(({name, price, url, id, color, isFavourite }) => <Card key={id} name={name} price={price} url={url} id={id} color={color} isFavourite={isFavourite} /> )}
            </div>
        </section>
    )

}

export default memo(ItemsContainer);